from application import app
from datetime import datetime

from flask import render_template, request, flash, json, jsonify
from flask_cors import CORS, cross_origin

import os
import re


#Landing Page
@app.route('/') 
@app.route('/index') 
@app.route('/home') 
def index_page(): 
    
    return render_template("index.html", 
        title="Object Recognition")
